﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace UOTT
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.loadDDDate();
                this.loadUottGrid();
            }
        }

        private void loadUottGrid()
        {
            string dataSrcDayStart = ddDayStart.SelectedValue;
            string dataSrcMonthStart = ddMonthStart.SelectedValue;
            string dataSrcYearStart = ddYearStart.SelectedValue;
            string dataSrcDateStart = dataSrcYearStart + "-" + dataSrcMonthStart + "-" + dataSrcDayStart;
            string dataSrcDayEnd = ddDayEnd.SelectedValue;
            string dataSrcMonthEnd = ddMonthEnd.SelectedValue;
            string dataSrcYearEnd = ddYearEnd.SelectedValue;
            string dataSrcDateEnd = dataSrcYearEnd + "-" + dataSrcMonthEnd + "-" + dataSrcDayEnd;
            DateTime srcDateStart, srcDateEnd;
            srcDateStart = DateTime.ParseExact(dataSrcDateStart, "yyyy-MM-d", CultureInfo.InvariantCulture);
            srcDateEnd = DateTime.ParseExact(dataSrcDateEnd, "yyyy-MM-d", CultureInfo.InvariantCulture);
            try
            {
                dbUOTTDataContext dbConn = new dbUOTTDataContext();
                var getData = from r in dbConn.vwOTLogs where r.DateTimeSlc >= srcDateStart & r.DateTimeSlc <= srcDateEnd select r;
                getData = getData.OrderByDescending(c => c.ID);
                if (getData.Count() > 0)
                {
                    var getDataOtx1 = getData.Where(s => s.OTx.Equals(1)).Sum(h => h.OT);
                    var getDataOtx15 = getData.Where(s => s.OTx.Equals(1.5)).Sum(h => h.OT);
                    var getDataOtx3 = getData.Where(s => s.OTx.Equals(3)).Sum(h => h.OT);
                    var getTotalHr = getData.Sum(h => h.OT);
                    var getTotalCost = getData.Sum(c => c.TotalCost);
                    lblTotalHr.Text = getTotalHr.Value.ToString("N");
                    lblTotalCost.Text = getTotalCost.Value.ToString("N");
                    lblX1Count.Text = getDataOtx1.ToString();
                    lblX15Count.Text = getDataOtx15.ToString();
                    lblX3Count.Text = getDataOtx3.ToString();
                    //decimal compCount = Convert.ToDecimal(getDataComp);
                    //decimal allCount = Convert.ToDecimal(getData.Count());
                    //decimal compPersentage = (compCount / allCount) * 100;
                    //lblAmdfSummary.Text = allCount.ToString();
                    //lblCompPersentage.Text = compPersentage.ToString("##.##");
                    GVUott.DataSource = getData.ToList();
                    GVUott.DataBind();
                    lblGridMsg.Text = "";
                }
                else
                {
                    GVUott.DataSource = null;
                    GVUott.DataBind();
                    lblGridMsg.Text = "Data not found !";
                }
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
                lblMsg.Text += exp.Message;
            }
        }

        private void loadDDDate()
        {
            DateTime now = DateTime.Now;
            string dayNowDate = now.Day.ToString();
            string monthNowDate = now.Month.ToString("d2");
            string yearNowDate = now.Year.ToString();
            var lastDayOfMonth = DateTime.DaysInMonth(now.Year, now.Month);

            ddDayStart.DataSource = DayArr();
            ddDayStart.DataTextField = "Value";
            ddDayStart.DataValueField = "Key";
            ddDayStart.SelectedValue = "1";
            ddDayStart.DataBind();
            ddMonthStart.DataSource = MonthArr();
            ddMonthStart.DataTextField = "Value";
            ddMonthStart.DataValueField = "Key";
            ddMonthStart.SelectedValue = monthNowDate;
            ddMonthStart.DataBind();
            ddYearStart.DataSource = YearArr();
            ddYearStart.DataTextField = "Value";
            ddYearStart.DataValueField = "Key";
            ddYearStart.SelectedValue = yearNowDate;
            ddYearStart.DataBind();

            ddDayEnd.DataSource = DayArr();
            ddDayEnd.DataTextField = "Value";
            ddDayEnd.DataValueField = "Key";
            ddDayEnd.SelectedValue = lastDayOfMonth.ToString();
            ddDayEnd.DataBind();
            ddMonthEnd.DataSource = MonthArr();
            ddMonthEnd.DataTextField = "Value";
            ddMonthEnd.DataValueField = "Key";
            ddMonthEnd.SelectedValue = monthNowDate;
            ddMonthEnd.DataBind();
            ddYearEnd.DataSource = YearArr();
            ddYearEnd.DataTextField = "Value";
            ddYearEnd.DataValueField = "Key";
            ddYearEnd.SelectedValue = yearNowDate;
            ddYearEnd.DataBind();
        }

        protected Dictionary<string, string> DayArr()
        {
            //ArrayList dataMachArr = new ArrayList();
            Dictionary<string, string> dataDayArr = new Dictionary<string, string>();
            try
            {
                int ic;
                for (ic = 1; ic <= 31; ic++)
                {
                    dataDayArr.Add(ic.ToString(), ic.ToString());
                }
                return (dataDayArr);
            }
            catch (Exception exp)
            {
                dataDayArr.Add("item 1", "Item 1");
                return (dataDayArr);
            }
        }

        protected Dictionary<string, string> MonthArr()
        {
            Dictionary<string, string> dataMonthArr = new Dictionary<string, string>();
            try
            {
                int ic;
                string mStr;
                for (ic = 1; ic <= 12; ic++)
                {
                    mStr = (ic < 10) ? "0" + ic.ToString() : ic.ToString();
                    dataMonthArr.Add(mStr, mStr);
                }
                return (dataMonthArr);
            }
            catch (Exception exp)
            {
                dataMonthArr.Add("item 1", "Item 1");
                return (dataMonthArr);
            }
        }

        protected Dictionary<string, string> YearArr()
        {
            Dictionary<string, string> dataYearArr = new Dictionary<string, string>();
            try
            {
                System.DateTime nowDate = DateTime.Now;
                int thisYear = nowDate.Year + 1;
                DateTime lastYearVal = DateTime.Today.AddYears(-1);
                int lastYear = lastYearVal.Year;
                if (thisYear > 0)
                {
                    for (int year = lastYear; year <= thisYear; year++)
                    {
                        dataYearArr.Add(year.ToString(), year.ToString());
                    }
                    return (dataYearArr);
                }
                else
                {
                    dataYearArr.Add(thisYear.ToString(), thisYear.ToString());
                    return (dataYearArr);
                }
            }
            catch (Exception exp)
            {
                dataYearArr.Add("ALL", "All");
                return (dataYearArr);
            }
        }

        protected void GVUott_Databound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblReason = (Label)e.Row.FindControl("lblReason");
                object objReson = DataBinder.Eval(e.Row.DataItem, "Reson");
                object objResonTxt = DataBinder.Eval(e.Row.DataItem, "ResonTxt");
                object objReason = DataBinder.Eval(e.Row.DataItem, "Reason");
                if (objReson.ToString() == "5")
                {
                    lblReason.Text = objReason + " : " + objResonTxt;
                }
            }
        }

        protected void GVUott_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.loadUottGrid();
            GVUott.PageIndex = e.NewPageIndex;
            GVUott.DataBind();
            //UpdatePanel2.Update();
        }

        protected void GVUott_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewDetail")
            {
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            this.loadUottGrid();
            UpdatePanel1.Update();
        }
    }
}