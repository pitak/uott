﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;

namespace UOTT.WS
{
    public partial class SaveData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool retVal = false;
             if ((Request.Form != null) && (Request.Form.Count > 0))
            {
                try
                {
                    //string strFilterID = Request.Form["dataASTKData"].ToString();
                    string dataUOTTData = Request.Form[0];

                    dbUOTTDataContext dbConn = new dbUOTTDataContext();
                    string dataSendUse = dataUOTTData.Replace("\\", "").Trim();
                    var json_serializer = new JavaScriptSerializer();
                    var jsonObj = (IDictionary<string, object>)json_serializer.DeserializeObject(dataSendUse);
                    var jsonAMDF = new JavaScriptSerializer().Serialize(jsonObj["dataUOTTData"]);
                    var jsonObjAMDF = (IDictionary<string, object>)json_serializer.DeserializeObject(jsonAMDF);

                    DateTime dataDate = DateTime.ParseExact(jsonObjAMDF["otDate"].ToString() + " 00:00", "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    string dataOtName = jsonObjAMDF["otName"].ToString();
                    string dataOtHr = jsonObjAMDF["otHR"].ToString();
                    string dataOtHrX = jsonObjAMDF["otHRX"].ToString();
                    string dataOtReason = jsonObjAMDF["otReason"].ToString();
                    string dataOtReasonTxt = jsonObjAMDF["otReasonTxt"].ToString();
                    if (true)
                    {
                        tbOTLog ot = new tbOTLog();
                        ot.Emp = Convert.ToInt32(dataOtName);
                        ot.OT = Convert.ToDecimal(dataOtHr);
                        ot.OTx = Convert.ToDecimal(dataOtHrX);
                        ot.Reson = Convert.ToInt32(dataOtReason);
                        ot.ResonTxt = dataOtReasonTxt.Trim();
                        ot.DateTimeSlc = dataDate;
                        ot.DateTimeLogs = DateTime.Now;
                        dbConn.tbOTLogs.InsertOnSubmit(ot);
                        dbConn.SubmitChanges();
                    }
                    dbConn.Connection.Close();
                    Response.Write(true);
                }
                catch 
                {
                    Response.Write(false);
                }
            }
        }
    }
}