﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

namespace UOTT.WS
{
    public partial class CreateUpdateData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.createUpdateFile();
        }

        private void createUpdateFile()
        {
            this.GetEmp();
            this.GetReason();
            this.GetOtX();
        }

        private void GetEmp()
        {
            dbUOTTDataContext dbConn = new dbUOTTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from g in dbConn.tbEmps
                           where g.Enable.Equals(true)
                           select new
                           {
                               ID = g.ID,
                               Data = g.ShortName
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_emp.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
                //Response.Write("สร้าง File Machine เรียบร้อย");
            }
            catch (Exception exp)
            {
            }
        }

        private void GetReason()
        {
            dbUOTTDataContext dbConn = new dbUOTTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from g in dbConn.tbReasons
                           where g.Enable.Equals(true)
                           select new
                           {
                               ID = g.ID,
                               Data = g.Reason
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_reason.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
                //Response.Write("สร้าง File System เรียบร้อย");
            }
            catch (Exception exp)
            {
            }
        }

        private void GetOtX()
        {
            dbUOTTDataContext dbConn = new dbUOTTDataContext();
            try
            {
                var serializer = new JavaScriptSerializer();
                var Data = from g in dbConn.tbOtXes
                           where g.Enable.Equals(true)
                           select new
                           {
                               ID = g.OtX,
                               Data = g.OtX
                           };
                string json = serializer.Serialize(Data);
                // Write the string to a file.
                System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("~") + "/StoreData/public_otx.txt");
                file.WriteLine(json);
                file.Close();
                dbConn.Connection.Close();
                //Response.Write("สร้าง File System เรียบร้อย");
            }
            catch (Exception exp)
            {
            }
        }
    }
}