﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace UOTT
{
    public partial class MonthlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.loadDDDate();
                this.loadDataMonthly();
            }
        }

        private void loadDDDate()
        {
            DateTime now = DateTime.Now;
            string dayNowDate = now.Day.ToString();
            string monthNowDate = now.Month.ToString("d2");
            string yearNowDate = now.Year.ToString();
            var lastDayOfMonth = DateTime.DaysInMonth(now.Year, now.Month);

            ddMonthSlc.DataSource = MonthArr();
            ddMonthSlc.DataTextField = "Value";
            ddMonthSlc.DataValueField = "Key";
            ddMonthSlc.SelectedValue = monthNowDate;
            ddMonthSlc.DataBind();

            ddYearSlc.DataSource = YearArr();
            ddYearSlc.DataTextField = "Value";
            ddYearSlc.DataValueField = "Key";
            ddYearSlc.SelectedValue = yearNowDate;
            ddYearSlc.DataBind();
        }

        protected Dictionary<string, string> MonthArr()
        {
            Dictionary<string, string> dataMonthArr = new Dictionary<string, string>();
            try
            {
                int ic;
                string mStr;
                for (ic = 1; ic <= 12; ic++)
                {
                    mStr = (ic < 10) ? "0" + ic.ToString() : ic.ToString();
                    dataMonthArr.Add(mStr, mStr);
                }
                return (dataMonthArr);
            }
            catch (Exception exp)
            {
                dataMonthArr.Add("item 1", "Item 1");
                return (dataMonthArr);
            }
        }

        protected Dictionary<string, string> YearArr()
        {
            Dictionary<string, string> dataYearArr = new Dictionary<string, string>();
            try
            {
                System.DateTime nowDate = DateTime.Now;
                int thisYear = nowDate.Year + 1;
                DateTime lastYearVal = DateTime.Today.AddYears(-1);
                int lastYear = lastYearVal.Year;
                if (thisYear > 0)
                {
                    for (int year = lastYear; year <= thisYear; year++)
                    {
                        dataYearArr.Add(year.ToString(), year.ToString());
                    }
                    return (dataYearArr);
                }
                else
                {
                    dataYearArr.Add(thisYear.ToString(), thisYear.ToString());
                    return (dataYearArr);
                }
            }
            catch (Exception exp)
            {
                dataYearArr.Add("ALL", "All");
                return (dataYearArr);
            }
        }

        protected void loadDataMonthly()
        {
            try
            {
                CultureInfo ci = new CultureInfo("en-us");
                string retDataDisp = null;
                string dataMonthlySlc = ddMonthSlc.SelectedValue;
                string dataYearSlc = ddYearSlc.SelectedValue;
                dbUOTTDataContext dbConn = new dbUOTTDataContext();
                var getDayInMonth = DateTime.DaysInMonth(Convert.ToInt32(dataYearSlc),Convert.ToInt32(dataMonthlySlc));
                var getDataMonthly = (from m in dbConn.vwOTLogs where m.DateTimeSlc.Value.Month.Equals(dataMonthlySlc) & m.DateTimeSlc.Value.Year.Equals(dataYearSlc) select m);
                if (getDataMonthly.Count() > 0)
                {
                    string retDay = null;
                    string retOtx1 = null;
                    string retOtx15 = null;
                    string retOtx3 = null;
                    string retTotalHr = null;
                    string retTotalCost= null;
                    retDay += "<tr style='background-color:#5CCCCC;'><td>Utility Overtime Monthly Report</td>";
                    retOtx1 += "<tr style='background-color:#67E667;'><td>OTx1</td>";
                    retOtx15 += "<tr style='background-color:#FFB273;'><td>OTx1.5</td>";
                    retOtx3 += "<tr style='background-color:#FF7373;'><td>OTx3</td>";
                    retTotalHr += "<tr style='background-color:#006363;color:white;font-weight:bold;'><td>Total OT (HR)</td>";
                    retTotalCost += "<tr style='background-color:#008500;color:white;font-weight:bold;'><td>Total Cost</td>";
                    for (int ctr = 1; ctr <= getDayInMonth; ctr++)
                    {
                        var getDataDiary = getDataMonthly.Where(d => d.DateTimeSlc.Value.Day.Equals(ctr));
                        var getOtx1 = getDataDiary.Where(d => d.OTx.Equals(1)).Sum(h => h.OT);
                        var getOtx15 = getDataDiary.Where(d => d.OTx.Equals(1.5)).Sum(h => h.OT);
                        var getOtx3 = getDataDiary.Where(d => d.OTx.Equals(3)).Sum(h => h.OT);
                        var getSumhr = getDataDiary.Sum(h => h.OT);
                        var getSumCost = getDataDiary.Sum(c => c.TotalCost);

                        string strOtx1 = (getOtx1 != null) ? getOtx1.Value.ToString("N01", ci) : "0";
                        string strOtx15 = (getOtx15 != null) ? getOtx15.Value.ToString("N01", ci) : "0";
                        string strOtx3 = (getOtx3 != null) ? getOtx3.Value.ToString("N01", ci) : "0";
                        string strSumhr = (getSumhr != null) ? getSumhr.Value.ToString("N01", ci) : "0";
                        string strSumCost = (getSumCost != null) ? getSumCost.Value.ToString("N01", ci) : "0";

                        retDay += "<td>" + ctr + "</td>";
                        retOtx1 += "<td>" + strOtx1 + "</td>";
                        retOtx15 += "<td>" + strOtx15 + "</td>";
                        retOtx3 += "<td>" + strOtx3 + "</td>";
                        retTotalHr += "<td>" + strSumhr + "</td>";
                        retTotalCost += "<td>" + strSumCost + "</td>";
                    }
                    retDay += "</tr>";
                    retOtx1 += "</tr>";
                    retOtx15 += "</tr>";
                    retOtx3 += "</tr>";
                    retTotalHr += "</tr>";
                    retTotalCost += "</tr>";
                    retDataDisp += "<table width='100%' cellpadding='0' cellspacing='0' border='0' style='font-size:10px;'>" + retDay + retOtx1 + retOtx15 + retOtx3 + retTotalHr + retTotalCost + "</table>";
                }
                dataDisp.InnerHtml = retDataDisp;
                dbConn.Connection.Close();
            }
            catch (Exception exp)
            {
                lblMsg.Text = exp.Message;
            }
        }

        protected void ddMonthSlc_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.loadDataMonthly();
            UpdatePanel1.Update();
        }

        protected void ddYearSlc_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.loadDataMonthly();
            UpdatePanel1.Update();
        }

    }
}