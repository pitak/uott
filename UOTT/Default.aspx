﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Main/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UOTT.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="art-layout-cell art-content">
	<div class="art-post">
		<div class="art-post-tl"></div>
		<div class="art-post-tr"></div>
		<div class="art-post-bl"></div>
		<div class="art-post-br"></div>
		<div class="art-post-tc"></div>
		<div class="art-post-bc"></div>
		<div class="art-post-cl"></div>
		<div class="art-post-cr"></div>
		<div class="art-post-cc"></div>
		<div class="art-post-body">
			<div class="art-post-inner art-article" style="width:954px;">
                <h2 class="art-postheader">Utility Overtime Report<asp:ScriptManager ID="ScriptManager1" 
                        runat="server">
                    </asp:ScriptManager>
                </h2>
                <table width="100%" cellpadding="0" cellspacing="0"> 
                    <tr>
                        <td width="10%">
                            Date :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddDayStart" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddMonthStart" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddYearStart" runat="server">
                            </asp:DropDownList>
                            &nbsp;TO&nbsp;
                            <asp:DropDownList ID="ddDayEnd" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddMonthEnd" runat="server">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddYearEnd" runat="server">
                            </asp:DropDownList>
                            &nbsp;

                            <asp:Button ID="btnView" runat="server" Text="View" onclick="btnView_Click" 
                                Width="100px" />

                        </td>
                        <td>
                            Total (HR) : 
                            <asp:Label ID="lblTotalHr" runat="server" Text="" 
                                style="font-weight: 700; color: #3333FF"></asp:Label><br />
                            Total Cost : 
                            <asp:Label ID="lblTotalCost" runat="server" Text="" 
                                style="font-weight: 700; color: #3333FF"></asp:Label><br />
                        </td>
                    </tr>
                </table>
				
				<div class="art-postcontent">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="text-align:center;">
                                <fieldset>
                                    <legend>Report page</legend>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>OTX1 = 
                                                <asp:Label ID="lblX1Count" runat="server" Text="0" 
                                                    style="color: #0000CC; font-weight: 700"></asp:Label></td>
                                            <td>OTX1.5 = 
                                                <asp:Label ID="lblX15Count" runat="server" Text="0" 
                                                    style="font-weight: 700; color: #0000CC"></asp:Label></td>
                                            <td>OTX3 = 
                                                <asp:Label ID="lblX3Count" runat="server" Text="0" 
                                                    style="font-weight: 700; color: #0000CC"></asp:Label></td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lblGridMsg" runat="server" Text=""></asp:Label>
                                    <asp:GridView ID="GVUott" runat="server" GridLines="Horizontal" 
                                        Width="100%"  
                                        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" PageSize="20" 
                                        OnPageIndexChanging="GVUott_OnPageIndexChanging" 
                                        OnRowDataBound = "GVUott_Databound"
                                        OnRowCommand="GVUott_OnRowCommand" 
                                        Font-Size="Smaller" 
                                    >
                                                                                                                                                                                                                                                                                <Columns>
                                        <asp:TemplateField HeaderText="ID" SortExpression="ID" HeaderStyle-Width="5%" Visible=false>
                                            <ItemTemplate>
                                                <asp:Label ID="lblID" runat="server" Text='<%#Bind("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="DateFound" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDateFound" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "DateTimeSlc", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name" SortExpression="ShortName">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShortName" runat="server" Text='<%#Bind("ShortName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OT (HR)" SortExpression="OT" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOT" runat="server" Text='<%#Bind("OT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="OT (X)" SortExpression="OTx" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOTx" runat="server" Text='<%#Bind("OTx") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total" SortExpression="Total" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <div style="text-align:right">
                                                <asp:Label ID="lblTotal" runat="server" Text='<%#Bind("Total") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cost" SortExpression="Cost" ItemStyle-HorizontalAlign="Right">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCost" runat="server" Text='<%#Bind("TotalCost") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reson" SortExpression="Reson">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReason" runat="server" Text='<%#Bind("Reason") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    </asp:GridView>
                                </fieldset>
				            </div>
                            <div style="text-align: center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                            </div>	
                        </ContentTemplate>
                    </asp:UpdatePanel>
				<div class="cleared"></div>
			</div>
			<div class="cleared"></div>
		</div>
	</div>
	<div class="cleared"></div>
</div>	
</div>
</asp:Content>
