﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Main/Main.Master" AutoEventWireup="true" CodeBehind="MonthlyReport.aspx.cs" Inherits="UOTT.MonthlyReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            color: #3333FF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="art-layout-cell art-content">
	<div class="art-post">
		<div class="art-post-tl"></div>
		<div class="art-post-tr"></div>
		<div class="art-post-bl"></div>
		<div class="art-post-br"></div>
		<div class="art-post-tc"></div>
		<div class="art-post-bc"></div>
		<div class="art-post-cl"></div>
		<div class="art-post-cr"></div>
		<div class="art-post-cc"></div>
		<div class="art-post-body">
			<div class="art-post-inner art-article" style="width:954px;">
                <h2 class="art-postheader">Utility Overtime Report<asp:ScriptManager ID="ScriptManager1" 
                        runat="server">
                    </asp:ScriptManager>
                </h2>
                <table width="100%" cellpadding="0" cellspacing="0"> 
                    <tr>
                        <td width="10%">
                            Month :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddMonthSlc" runat="server" OnSelectedIndexChanged="ddMonthSlc_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                            &nbsp;
                            Year : 
                            &nbsp;
                            <asp:DropDownList ID="ddYearSlc" runat="server" OnSelectedIndexChanged="ddYearSlc_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
				
				<div class="art-postcontent">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="text-align:center;">
                                <fieldset>
                                    <legend class="style1"><strong>Utility Overtime Monthly Report</strong></legend>
                                    <div id="dataDisp" runat="server"></div>
                                </fieldset>
				            </div>
                            <div style="text-align: center">
                                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                            </div>	
                        </ContentTemplate>
                    </asp:UpdatePanel>
				<div class="cleared"></div>
			</div>
			<div class="cleared"></div>
		</div>
	</div>
	<div class="cleared"></div>
</div>	
</asp:Content>
